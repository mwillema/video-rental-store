FROM maven:3.5.3-jdk-8 as BUILD
COPY src /app/src
COPY pom.xml /app
RUN mvn -f /app clean install -Pintegration-tests

FROM openjdk:8-jre-alpine
ENV JAR_FILE video-rental-store-1.0.0-SNAPSHOT.jar
COPY --from=BUILD /app/target/${JAR_FILE} /app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
