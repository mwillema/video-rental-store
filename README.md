# Video Rental Store

## Table of Contents

1. [Introduction](#markdown-header-introduction)
2. [Domain Model](#markdown-header-domain-model)
3. [REST API](#markdown-header-rest-api)
    1. [Command API](#markdown-header-command-api)
    2. [Query API](#markdown-header-query-api)
    3. [A more RESTful API](#markdown-header-a-more-restful-api)
4. [Architecture](#markdown-header-architecture)
5. [Javadoc](#markdown-header-javadoc)
6. [Defensive Programming](#markdown-header-defensive-programming)
7. [Tests](#markdown-header-tests)
    1. [Unit Tests](#markdown-header-unit-tests)
    2. [Integration Tests](#markdown-header-integration-tests)
8. [Usage](#markdown-header-usage)
    1. [Build and Run](#markdown-header-build-and-run)
    2. [Initial Data](#markdown-header-initial-data)
    3. [Demo](#markdown-header-demo)
    4. [Technology Stack](#markdown-header-technology-stack)
9. [Conclusion](#markdown-header-conclusion)
10. [Bibliography](#markdown-header-bibliography)

## Introduction

I could have done the project by developing a CRUD application using a classic stack such as Spring + JPA.
But what is the point? This challenge is a showcase of my skills. As such, I developed it by using Domain-Driven Design (DDD), Command Query Responsability Segregation (CQRS) and Event Sourcing (ES).

Besides, it is also how I lead the development of the products in the start-up company I am working for. Time-to-market is critical, new features pop up quickly, requirements change at light speed, and new customers need to be convinced that we are serious about what we do. So trading quality for dirtiness is not an option, even with important time constraints. Finally our products must be resilient and able to cope with increasing load quickly.

With time and experience, I have come up with an architecture that allows fast development by reducing boilerplate coding to a minimum while focusing on the business value the software brings to the company. It offers an environment which in enjoyable to work in, which allows to apply CQRS + ES easily, and that can be scaled up in the future if or when needed.

I like to start simple, having the KISS principle in mind. At the same time I like to be ready to tackle more complex problems. This is what I call an "open architecture", that is an architecture that lets as many doors as possible opened for future evolutions. A software architect is like a chess player, he must always have a couple of shots in advance, without knowing what the future will be like. In essence, he must be a little bit visionary :-)

## Domain Model

The Domain Model is designed according to the DDD tactical modeling best practices, such as:

* Model true invariants in consistency boundaries.
* Design small aggregates.
* Reference other aggregates by identity.
* ...

Entity identities are globally unique. In addition they are structured in order to convey information about which entity they refer to, when they were created and in which bounded context.

For example `VRS-F-2018-11-15-5E5CDACA3478` where:

* `VRS` is the key identifying the _Video Rental Store_ bounded context.
* `F` is the code identifying the _Film_ entity.
* `2018-11-15` is the creation date of the entity.
* `5E5CDACA3478` is the random part of the identity that guarantees its unicity.

Below is an overview of the Domain Model. Since _the code is the model_ and vice versa, the following UML class diagram does not go into much details. Rather its goal is to provide a high-level view of the Domain Model. For more details see the Java classes under the [`domain`](src/main/java/com/marcowillemart/videorentalstore/domain) package.

It is similar to viewing a city from a plane versus walking into its streets. You will not see the same things but both views can be useful. I use the following diagram when I first sketch a Domain Model and then to talk about it or explain it to other folks. It has proved quite useful in the past.

![Domain Model](doc/domain-model.png)

The DDD concepts and practices mentioned above are presented in [Evans, 2004] and [Vernon, 2013].

## REST API

The REST API is split in two parts. The API for handling commands and the API for handling queries.

Hang on a moment. This sounds weird. Indeed, the architectural choices have a clear impact on the API, which does not sound like a good practice. Also this does not seem very RESTful, does it?

While this is true, I have good reasons for making such a choice:

* The command and query sides could be deployed separately and thus exposed with a different base URL.
* Commands are defined using [Protocol Buffers](https://developers.google.com/protocol-buffers/) and sent to the `commands` resource. As such, as soon as a new command is supported by the system, the REST API also supports it without any further development.
* In its simplest form, the `commands` resource is synchronous, meaning that when the request returns, the command has either be processed or rejected. In the future, be it necessary, the `commands` resource could become an asynchronous queue of commands such as in [Betts et al. 2013].
* [Spring REST Data](https://spring.io/projects/spring-data-rest) is leveraged to expose the Query Model through REST. Similarly to the commands, when the Query Model changes, it is already accessible through the REST API without furter ado.

In the past these reasons have allowed me to successfully lead the development of products with a very short time-to-market and versatile requirements. That being said, if a more RESTful API is nonetheless required, an additional component providing the API and routing the calls to the appropriate command or query side could be deployed.

### Command API

The command API consists in two base resources:

1. `identities` that generates new identities
2. `commands` that processes commands

#### Identities

| Method | Resource               | Description |
| ------ | -----------------------| ----------- |
| POST   | /identities/customer   | Returns the generated identity for customers as a plain string along with a 201 HTTP status code.
| POST   | /identities/film       | Returns the generated identity for films as a plain string along with a 201 HTTP status code.
| POST   | /identities/rental     | Returns the generated identity for rentals as a plain string along with a 201 HTTP status code.

#### Commands

Commands are encoded as JSON requests which must contain a `@type` attribute:

```json
{
    "@type" : ... // the command type
    "..." : "..." // other attributes according to command type
}
```

Once encoded commands are then sent with:

```rest
PUT /commands
```

Note that even though I talked about JSON, the `application/x-protobuf` binary format is also supported.

The definition of commands along with their documentation can be found in [`command.proto`](src/main/protobuf/application/command.proto). So for the sake of brevity I am not duplicating them here. Nonetheless you will find below a list of supported commands along with an example.

* CreateCustomer (The creation of a new customer)

```json
{
  "@type": "marcowillemart.com/videorentalstore.application.CreateCustomer",
  "customerId": "VRS-C-2018-11-15-8301E66C7DBA",
  "name": "Johnny Depp"
}
```

* GrantBonusPoints (A customer is granted some bonus point when renting a film)

```json
{
  "@type": "marcowillemart.com/videorentalstore.application.GrantBonusPoints",
  "customerId": "VRS-C-2018-11-18-C9016FD74C03",
  "bonusPoints": 3
}
```

* AddFilm (The addition of a new film in the inventory)

```json
{
  "@type": "marcowillemart.com/videorentalstore.application.AddFilm",
  "filmId": "VRS-F-2018-11-18-F4F88231F814",
  "title": "The Sisters Brothers",
  "type": "NEW"
}
```

* StartRental (The start of a rental by a customer which will allow him to rent films)

```json
{
  "@type": "marcowillemart.com/videorentalstore.application.StartRental",
  "rentalId": "VRS-R-2018-11-18-6CA1DFC69720",
  "customerId": "VRS-C-2018-11-18-C9016FD74C03"
}
```

* RentFilm (The renting of a specific film by a customer for a given number of days)

```json
{
  "@type": "marcowillemart.com/videorentalstore.application.RentFilm",
  "rentalId": "VRS-R-2018-11-18-6CA1DFC69720",
  "filmId": "VRS-F-2018-11-18-F4F88231F814",
  "nbOfDays": 3
}
```

* ReturnFilm (The return of a rented film by a customer)

```json
{
  "@type": "marcowillemart.com/videorentalstore.application.ReturnFilm",
  "rentalId": "VRS-R-2018-11-18-6CA1DFC69720",
  "filmId": "VRS-F-2018-11-18-F4F88231F814",
  "returnDate": "2018-11-25"
}
```

### Query API

Get the inventory of films:

```rest
GET /films
```

Get a given film, including its title and type:

```rest
GET /films/{filmId}
```

Example:

```rest
GET /films/VRS-F-2018-11-18-F4F88231F814
```

```json
{
  "filmId": "VRS-F-2018-11-18-F4F88231F814",
  "title": "The Sisters Brothers",
  "type": "New release"
}
```

Get the customers of the application:

```rest
GET /customers
```

Get a given customer, including his bonus points:

```rest
GET /customers/{customerId}
```

Example:

```rest
GET /customers/VRS-C-2018-11-18-C9016FD74C03
```

```json
{
  "customerId": "VRS-C-2018-11-18-C9016FD74C03",
  "name": "Johnny Depp",
  "bonusPoints": 2
}
```

Get the rentals of a customer:

```rest
GET /customers/{customerId}/rentals
```

Get a given rental of a given customer, including the total price, total late charge, and for each rented film the number of rented days, the number of extra days, the price and the possible late charge:

```rest
GET /customers/{customerId}/rentals/{rentalId}
```

Example:

```rest
GET /customers/VRS-C-2018-11-18-C9016FD74C03/rentals/VRS-R-2018-11-18-6CA1DFC69720
```

```json
{
  "rentalId": "VRS-R-2018-11-18-6CA1DFC69720",
  "totalPrice": 200,
  "totalCharge": 120,
  "rentedFilms": [{
      "filmId": "VRS-F-2018-11-15-2D9F4CE85718",
      "days": 2,
      "price": 80,
      "extraDays": 0,
      "lateCharge": 0,
      "status": "returned"
    }, {
      "filmId": "VRS-F-2018-11-18-F4F88231F814",
      "days": 3,
      "price": 120,
      "extraDays": 3,
      "lateCharge": 120,
      "status": "returned"
    }]
}
```

### A more RESTful API

Be the need arises, and also to show that I can design RESTful APIs ;-), here is an addition to the Query API that brings back the _commands_ into a unique API.

* AddFilm

```rest
POST /films
```

* CreateCustomer

```rest
POST /customers
```

* StartRental

```rest
POST /customers/{customerId}/rentals
```

* RentFilm

```rest
PUT /customers/{customerId}/rentals/{rentalId}/films/{filmId}
```

* ReturnFilm

```rest
POST /customers/{customerId}/rentals/{rentalId}/films/{filmId}/return
```

The above resources and operations are designed according to [Masse, 2011].

## Architecture

The application is implemented using the _Hexagonal_ architecture style, also known as _Ports and Adapters_. It is more than an architecture, this is why I refer to it as an _architecture style_. Actually many types of architectures can be implemented from it, such as Event-Driven Architecture (EDA), Service-Oriented Architecture (SOA), Multi-Tiers architecture, etc. A more complete explanation can be found in [Vernon, 2013].

By using the Hexagonal architecture style, I do not constraint myself to much about future architectural changes and evolutions. As explained in the introduction, this is what I consider an _open architecture_.

The following picture describes the mapping between the different parts of the Hexagonal architecture style and the Java packages in the Video Rental Store application.

The _Domain Model_ is implemented under the [`domain`](src/main/java/com/marcowillemart/videorentalstore/domain) package. Each _Aggregate_ has its own subpackage named after itself. For example the _Film_ aggregate is implemented in [`domain.film`](src/main/java/com/marcowillemart/videorentalstore/domain/film). These packages form the core of the application and this is where the business logic lives. It is also the most important part of the application since it carries business value. It tackles domain complexity but is free of any technical complexity.

All third-party dependencies and technical implementations are found in the _Infrastructure_ layer, implemented in the subpackages under [`infrastructure`](src/main/java/com/marcowillemart/videorentalstore/infrastructure). For example [`infrastructure.repository`](src/main/java/com/marcowillemart/videorentalstore/infrastructure/repository) hosts the implementation of the different _Repositories_, such as [`EventStoreFilmRepository`](src/main/java/com/marcowillemart/videorentalstore/infrastructure/repository/EventStoreFilmRepository.java).

The _Application_ layer is a collection of thin stateless services that isolate the _Domain Model_ from the outside. These services are called _Application Services_ and they are responsible for handling cross-cutting concerns such as transactions, security, logging, and also coordinate the calls to the _Domain Model_. This layer is found under the [`application`](src/main/java/com/marcowillemart/videorentalstore/application) package.

Finally the REST API, a _Port_, is mapped to the application using an _Adapter_, which is implemented in the [`ws`](src/main/java/com/marcowillemart/videorentalstore/ws) package. Other _Adapters_ could be found in this layer.

For the terminology used here see [Vernon, 2013] for more details.

![Architecture](doc/architecture.png)

## Javadoc

By looking at the code you will notice that I do not use all the standard Javadoc  tags nor do I limit myself to these tags. I find that using additional tags allows me to have a more structured code documentation. In addition, it is a semi-formal way to apply Design by Contracts principles, as researched in [Liskov and Guttag, 2000] and taught in [Jackson and Devadas, 2005] at MIT.

For more information about how it works and in particular my experience and the way I use it, see my blog [Willemart, 2016]. In particular the following articles may be of interest to better understand the code of the Video Rental Store application:

* [Writing good method documentation](http://consciousprogrammer.com/writing-good-method-documentation/)
* [Class specification: the effortless panoramic view](http://consciousprogrammer.com/class-specification-the-effortless-panoramic-view/)
* [Beware of illicit representations](http://consciousprogrammer.com/beware-of-illicit-representations/)
* [Checking representation invariants at runtime](http://consciousprogrammer.com/checking-representation-invariants-at-runtime/)

## Defensive Programming

I am always in favor of defensive programming. This consists mostly in checking _preconditions_ at the start of public methods and checking _rep invariants_, also called _class invariants_, just before the end of public methods. See [Willemart, 2016] for more information about how I apply runtime assertion checking.

When an assertion fails, in Java it is recommended to use the standard `IllegalArgumentException`, `IllegalStateException`,... [Bloch, 2018]. However I prefer throwing my own unchecked / runtime exception which is `FailureException`. When this exception is thrown, a message with the classe, method and line number where it originates is automatically computed. This provides the following benefits:

* It is very easy to throw such an exception since one does not need to write any error message. Even a simple `throw new FailureException()` suffices, since the message attached to the exception is automatically built as explained above. So writing assertions throwing this exception is as simple as writing `Assert.notNull(filmId)`. It takes a couple of seconds to add assertions and so no one has the excuse of finding it time consuming or boring.
* When I see an error log with a `FailureException` I'm reassured since I know this is _normal_, is something under control, a precondition or rep invariant that is violated. Usually, heading to the place in the code specified in the message will tell me what was wrong and fixing the issue takes no time (fail fast mechanism).
* Sometimes the stack trace is not printed in logs. This can be for security reasons. In such a case we cannot know where the exception was thrown. The message of the `FailureException` tells me exactly what assertion failed and where it broke, so this is not an issue.

For example, when writing a test case I got the following exception:

```java
FailureException: equals assertion broken at com.marcowillemart.videorentalstore.domain.rental.Rental.rentFilm(Rental.java:109)
```

Heading back to the specified code line I found out the broken assertion:

```java
Assert.equals(LocalDate.now(), startDate);
```

As part of my unit test I was renting a film in the past in order to return it today. Problem found, problem fixed!

## Tests

### Unit Tests

The classes of the Domain Model, that is the classes in the subpackages under [`domain`](src/main/java/com/marcowillemart/videorentalstore/domain), are well tested. Indeed since this is where the business logic and thus the value of the application resides, it is important to have good tests. These tests are _unit tests_, meaning they execute very quickly and they should be run with each build of the application.

Since the Domain Model does not contain technical code, for example code that connects to an SMTP server in order to send an e-mail or that connects to a database in order to persist something, it is very easy to write tests with mocked or stubed dependencies if needed. The dependencies may change in the future, it will not affect the Domain Model and its tests.

Last but not least, these tests are usually _black box_ tests, meaning that they test the various paths found in the specifications of classes and methods. Once again, this allows one to change the implementation without breaking the tests, which would have been more likely the case with _white box_ tests.

Unit tests are classes ending with `Test` such as [`RentalTest`](src/test/java/com/marcowillemart/videorentalstore/domain/rental/RentalTest.java). Unit tests are run whenever the application is build, for instance with `mvn clean package`.

### Integration Tests

There also exist _integration tests_ which use the full application. They can be seen as _end-to-end_ tests since they test the application from one end (sending a command) to the other (persisting the changes). During these tests a Spring context is initialized, the different beans instantiated, etc. Integration tests are written at the _Application Services_ level since they represent the actual entry point of the application. During these tests we assert that the expected _Domain Events_ are published, since the application is implemented using _Event Sourcing_.

Integration tests are classes ending with `IT` such as [`RentalApplicationServiceIT`](src/test/java/com/marcowillemart/videorentalstore/application/RentalApplicationServiceIT.java). They can be run with `mvn clean verify -Pintegration-tests`.

Note that as part of these tests we could verify the Query Model is correctly updated in response to Domain Events being published. However there are a couple of reasons why I do not like mixing together tests on the Domain Model with tests on the Query Model:

* The Query Model could be _eventually consistent_ with the Domain Model. While this is not the case in the Video Rental Store application for the time being, it could be in an hypothetic future. In such a case we would have to refactor our tests.
* The Query Model and Domain Model could be deployed in different components. As such our current tests would need to be refactored as well in order to separate the code sending commands and verifying published events with the code verifying the Query Model.
* Projections are often trivial or at least not that complicated. Also, by nature, both the Query Model and Projections can be updated quite easily. Since the Query Model can always be rebuilt from the Event Store, it is easily disposable. One could even wonder whether it is worth testing the Projections and the Query Model at all.

In conclusion, while I __always__ write _integration tests_ for every command of the application, I either write seperate _integration tests_ for the Query Model or not bother at all writing such tests.

## Usage

### Build and Run

The project has been built in the following environments:

* OS: Mac, Ubuntu
* Java 8: Oracle, OpenJDK
* Maven: 3.3.3, 3.5.3

During development, the app can be run by using the Spring Boot plugin:

```bash
mvn spring-boot:run
```

Otherwise, to build the app and run the unit tests:

```bash
mvn clean package
```

Alternatively, to build the app and run both the unit and integration tests:

```bash
mvn clean install -Pintegration-tests
```

To run the JAR file once the app has been built:

```bash
java -jar target/video-rental-store-1.0.0-SNAPSHOT.jar
```

You can also build a Docker image once the app has been built:

```bash
mvn docker:build
```

And run the app in a Docker container with either of the following commands:

```bash
mvn docker:run
```

```bash
docker run --rm -it -p 8080:8080 video-rental-store:1.0.0-SNAPSHOT
```

If you prefer to build and run the app regardless of your build environment, you can also do it through Docker with Docker Compose.

Note that this has been tested with the following environment:

* Docker 18.06.1-ce
* Docker Compose 1.22.0

To build the app:

```bash
docker-compose build app
```

Note this can take some time since all the Maven dependencies need to be fetch every time the app is built.

To run the app once its image has been built:

```bash
docker-compose up app
```

Once up and running, the Video Rental Store API is accessible on <http://localhost:8080>.

### Initial Data

The application is initialized with some films and customers. These data are already present in the Event Store when the application is started. It also demonstrates that when the Query Model does not exist yet or its schema is out-of-date, the Projections are restarted and the events present in the Event Store replayed.

### Demo

I have written a script that plays a demo scenario using the REST API of the Video Rental Store application. It implements the examples of price calculations I have received.

If you run on MacOS:

1. Start the application using one of the methods described earlier
2. Once the application is running, execute the demo script: `sh demo/demo_mac.sh`

Regarding of your OS you can play the demo using Docker:

1. Build a Docker image of the application using one of the methods described earlier
2. Build the Docker image of the demo script: `docker-compose build demo`
3. Start the application: `docker-compose up app`
4. Once the application is running, execute the demo script: `docker-compose run demo`

You can observe the logs of the application while the demo script is running. You can also inspect the Query Model through its REST API by opening <http://localhost:8080>. At this address you will find a _HAL Browser_ that will help you navigate through the Query API.

### Technology Stack

Last but not least, the main components of the technology stack:

* Maven 3.x
* Java 8
* Spring Boot 2.x
* Spring Framework 5.x
* [Common](https://gitlab.com/mwillema/common) (personal project)
* [Event Store](https://gitlab.com/mwillema/eventstore) (personal project)

I used my own libraries to quickly develop this application with CQRS + ES.

For the Command Side, all the base classes providing Event Sourcing capabilities can be found in [Common Event](https://gitlab.com/mwillema/common/tree/master/common-event/src/main/java/com/marcowillemart/common). The [Event Store Client Embedded](https://gitlab.com/mwillema/eventstore/tree/master/eventstore-client-embedded) provides the embedded Event Store used in the application. [Event Store Engine](https://gitlab.com/mwillema/eventstore/tree/master/eventstore-engine) is the core of the Event Store.

For the Query Side, I used a combination of [Spring Data](https://spring.io/projects/spring-data), [Hibernate](http://hibernate.org) and [Lombok](https://projectlombok.org) in order to have a Query Model that is rebuilt automatically at deployment time if its underlying schema is no longer valid. This saves a lot of time and is very handy when migrating. The building blocks providing these functionalities can be found in [Common Projection](https://gitlab.com/mwillema/common/tree/master/common-projection/src/main/java/com/marcowillemart/common).

Access control is not implemented since it was not mentioned in the coding challenge. However this is a requirement that would most likely been asked by stakeholders in the future. Since the Spring Framework is used, it is very easy to integrate [Spring Security](https://spring.io/projects/spring-security) that would allow to add authentication and authorization to the application.

## Conclusion

With this coding challenge, I have taken the opportunity to not only show my skills, but also to share my view, experience and opinion on software craftmanship, something I am really passionate about.

Even though this README is already quite long, I have only scratched the surface of many of the discussed topics, and not even talked about many others. This would probably require a hole book :-) In particular I have barely talked about how I technically implement my design decisions, including CQRS + ES. This is in part because I have used my own building blocks (see my [GitLab account](https://gitlab.com/mwillema)) and talking about them in depth is outside the scope of this coding challenge.

That being said, I am more than willing to continue the discussion about any of these topics or anything else, so feel free to ask :-)

## Bibliography

[Betts et al. 2013] Dominic Betts, Julian Dominguez, Grigori Melnik, Fernando Simonazzi, and Mani Subramanian. 2013. _Exploring CQRS and Event Sourcing: A Journey into High Scalability, Availability, and Maintainability with Windows Azure (1st ed.)._ Microsoft patterns & practices.

[Block, 2018] Bloch, J. (2018), Effective Java , Addison-Wesley , Boston, MA .

[Evans, 2004] Evans, E. (2004), _Domain-Driven Design: Tackling Complexity in the Heart of Software_ , Addison-Wesley .

[Jackson and Devadas, 2005] Jackson, D. and Devadas, S. (2005). _Laboratory in Software Engineering_ , <https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-170-laboratory-in-software-engineering-fall-2005/>

[Liskov and Guttag, 2000] Liskov, B. and Guttag, J. (2000). _Program Development in Java: Abstraction, Specification, and Object-Oriented Design._ Addison-Wesley Longman Publishing Co., Inc., Boston, MA, USA, 1st edition.

[Masse, 2011] Masse, M. (2011), _REST API Design Rulebook_ , O'Reilly Media .

[Meszaros, 2007] Meszaros, G. (2007), _XUnit Test Patterns: Refactoring Test Code_ , Addison-Wesley .

[Vernon, 2013] Vernon, V. (2013), _Implementing Domain-Driven Design_ , Addison-Wesley , Upper Saddle River, NJ .

[Willemart, 2016] Wilemart, M. (2016), _The Conscious Programmer_ , <http://consciousprogrammer.com>
