#!/usr/bin/env bash

function start_rental {
    curl --request PUT \
        --url http://app:8080/commands \
        --header 'content-type: application/json' \
        --data '{
	        "@type": "marcowillemart.com/videorentalstore.application.StartRental",
	        "rentalId": "'$rental_id'",
		    "customerId": "'$customer_id'"
        }'
}

function rent_film {
    curl --request PUT \
        --url http://app:8080/commands \
        --header 'content-type: application/json' \
        --data '{
		    "@type": "marcowillemart.com/videorentalstore.application.RentFilm",
	        "rentalId": "'$rental_id'",
		    "filmId": "'$1'",
	        "nbOfDays": '$2'
        }'
}

function return_film {
    local return_date=$(date -d "$2 days" -I)

    curl --request PUT \
        --url http://app:8080/commands \
        --header 'content-type: application/json' \
        --data '{
		    "@type": "marcowillemart.com/videorentalstore.application.ReturnFilm",
	        "rentalId": "'$rental_id'",
		    "filmId": "'$1'",
	        "returnDate": "'$return_date'"
        }'
}

function get_rental {
    curl -s --request GET --url http://app:8080/customers/$customer_id/rentals/$rental_id
}

function print_rented_film {
    local rented_film=$(get_rental | jq '.rentedFilms[]  | select(.filmId == "'$2'")')
    local days=$(echo $rented_film | jq '.days')
    local price=$(echo $rented_film | jq '.price')
    echo "$1 $days days $price SEK"
}

function print_returned_film {
    local rented_film=$(get_rental | jq '.rentedFilms[]  | select(.filmId == "'$2'")')
    local extra_days=$(echo $rented_film | jq '.extraDays')
    local late_charge=$(echo $rented_film | jq '.lateCharge')
    echo "$1 $extra_days extra days $late_charge SEK"
}

customer_id=VRS-C-2018-11-15-8301E66C7DBA

rental_id=$(curl -s --request POST --url http://app:8080/identities/rental)

# Start Rental
start_rental

echo "1) Renting Films"
echo

# Rent Matrix 11
rent_film "VRS-F-2018-11-15-23294EC11B0E" 1
print_rented_film "Matrix 11 (New release)" "VRS-F-2018-11-15-23294EC11B0E"

# Rent Spider Man
rent_film "VRS-F-2018-11-15-FAB657CA5922" 5
print_rented_film "Spider Man (Regular rental)" "VRS-F-2018-11-15-FAB657CA5922"

# Rent Spider Man 2
rent_film "VRS-F-2018-11-15-F90C0475AA59" 2
print_rented_film "Spider Man 2 (Regular rental)" "VRS-F-2018-11-15-F90C0475AA59"

# Rent Out of Africa
rent_film "VRS-F-2018-11-15-DDE9C93B72B4" 7
print_rented_film "Out of Africa (Old film)" "VRS-F-2018-11-15-DDE9C93B72B4"

rental=$(get_rental)
total_price=$(echo $rental | jq '.totalPrice')
echo
echo "TOTAL PRICE: $total_price SEK"
echo
echo

echo "2) Returning Films"
echo

# Return Matrix 11
return_film "VRS-F-2018-11-15-23294EC11B0E" 3
print_returned_film "Matrix 11 (New release)" "VRS-F-2018-11-15-23294EC11B0E"

# Return Spider Man
return_film "VRS-F-2018-11-15-FAB657CA5922" 6
print_returned_film "Spider Man (Regular rental)" "VRS-F-2018-11-15-FAB657CA5922"

# Return Spider Man 2
return_film "VRS-F-2018-11-15-F90C0475AA59" 2
print_returned_film "Spider Man 2 (Regular rental)" "VRS-F-2018-11-15-F90C0475AA59"

# Return Out of Africa
return_film "VRS-F-2018-11-15-DDE9C93B72B4" 7
print_returned_film "Out of Africa (Old film)" "VRS-F-2018-11-15-DDE9C93B72B4"

rental=$(get_rental)
total_late_charge=$(echo $rental | jq '.totalCharge')
echo
echo "TOTAL LATE CHARGE: $total_late_charge SEK"
