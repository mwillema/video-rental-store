#!/usr/bin/env bash

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.CreateCustomer",
		"customerId": "VRS-C-2018-11-15-8301E66C7DBA",
    "name": "Clint Eastwood"
    }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.CreateCustomer",
		"customerId": "VRS-C-2018-11-15-B44F9D215E0D",
    "name": "Sergio Leone"
    }'
