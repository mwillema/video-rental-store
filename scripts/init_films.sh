#!/usr/bin/env bash

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-23294EC11B0E",
		"title": "Matrix 11",
		"type": "NEW"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-FAB657CA5922",
		"title": "Spider Man",
		"type": "REGULAR"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-F90C0475AA59",
		"title": "Spider Man 2",
		"type": "REGULAR"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-DDE9C93B72B4",
		"title": "Out of Africa",
		"type": "OLD"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-2D9F4CE85718",
		"title": "The Last Jedi",
		"type": "NEW"
  }'

  curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-DE404AE828E6",
		"title": "The Force Awakens",
		"type": "REGULAR"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-708CE07D6D40",
		"title": "A New Hope",
		"type": "OLD"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-5E5CDACA3478",
		"title": "The Empire Strikes Back",
		"type": "OLD"
  }'

curl --request PUT \
  --url http://localhost:8080/commands \
  --header 'content-type: application/json' \
  --data '{
		"@type": "marcowillemart.com/videorentalstore.application.AddFilm",
		"filmId": "VRS-F-2018-11-15-39F2832BFE8C",
		"title": "Return of the Jedi",
		"type": "OLD"
  }'
