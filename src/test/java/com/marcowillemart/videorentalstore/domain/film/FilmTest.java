package com.marcowillemart.videorentalstore.domain.film;

import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class FilmTest {

    private Film target;

    @Test
    public void testAddFilm() {
        // Setup
        FilmId filmId = FilmId.newFilmId();
        String title = createAnonymousName();
        FilmType type = FilmType.OLD;

        // Exercise
        target = new Film(filmId, title, type);

        // Verify
        assertEquals(filmId, target.filmId());
        assertEquals(title, target.title());
        assertEquals(type, target.type());
    }
}
