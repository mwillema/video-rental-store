package com.marcowillemart.videorentalstore.domain.film;

import static org.junit.Assert.*;
import org.junit.Test;

public class FilmTypeTest {

    private FilmType target;

    @Test
    public void testPrice_new_oneDay() {
        // Setup
        target = FilmType.NEW;
        int expected = Price.PREMIUM.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(1));
    }

    @Test
    public void testPrice_new_threeDays() {
        // Setup
        target = FilmType.NEW;
        int expected = 3 * Price.PREMIUM.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(3));
    }

    @Test
    public void testPrice_regular_oneDay() {
        // Setup
        target = FilmType.REGULAR;
        int expected = Price.BASIC.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(1));
    }

    @Test
    public void testPrice_regular_threeDays() {
        // Setup
        target = FilmType.REGULAR;
        int expected = Price.BASIC.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(3));
    }

    @Test
    public void testPrice_regular_fiveDays() {
        // Setup
        target = FilmType.REGULAR;
        int expected = Price.BASIC.value() + 2 * Price.BASIC.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(5));
    }

    @Test
    public void testPrice_old_oneDay() {
        // Setup
        target = FilmType.OLD;
        int expected = Price.BASIC.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(1));
    }

    @Test
    public void testPrice_old_fiveDays() {
        // Setup
        target = FilmType.OLD;
        int expected = Price.BASIC.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(5));
    }

    @Test
    public void testPrice_old_heightDays() {
        // Setup
        target = FilmType.OLD;
        int expected = Price.BASIC.value() + 3 * Price.BASIC.value();

        // Exercise & Verify
        assertEquals(expected, target.priceFor(8));
    }

    @Test
    public void testBonusPoints_new() {
        // Setup
        target = FilmType.NEW;

        // Exercise & Verify
        assertEquals(2, target.bonusPoints());
    }

    @Test
    public void testBonusPoints_regular() {
        // Setup
        target = FilmType.REGULAR;

        // Exercise & Verify
        assertEquals(1, target.bonusPoints());
    }

    @Test
    public void testBonusPoints_old() {
        // Setup
        target = FilmType.OLD;

        // Exercise & Verify
        assertEquals(1, target.bonusPoints());
    }
}
