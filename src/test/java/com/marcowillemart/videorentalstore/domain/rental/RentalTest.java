package com.marcowillemart.videorentalstore.domain.rental;

import com.marcowillemart.common.test.AbstractMockTest;
import com.marcowillemart.videorentalstore.domain.DateProvider;
import com.marcowillemart.videorentalstore.domain.customer.Customer;
import com.marcowillemart.videorentalstore.domain.film.Film;
import com.marcowillemart.videorentalstore.domain.film.FilmType;
import static com.marcowillemart.videorentalstore.test.Tests.*;
import java.time.LocalDate;
import static org.jmock.AbstractExpectations.*;
import org.jmock.Expectations;
import org.jmock.auto.Mock;
import static org.junit.Assert.*;
import org.junit.Test;

public class RentalTest extends AbstractMockTest {

    private Rental target;

    @Mock
    private DateProvider dateProvider;

    @Test
    public void testStartRental() {
        // Setup
        RentalId rentalId = RentalId.newRentalId();
        Customer customer = createAnonymousCustomer();
        LocalDate startDate = LocalDate.now();

        // Configure
        context.checking(new Expectations() {{
            oneOf(dateProvider).today(); will(returnValue(startDate));
        }});

        // Exercise
        target = new Rental(rentalId, customer, dateProvider);

        // Verify
        assertEquals(rentalId, target.rentalId());
        assertEquals(customer.customerId(), target.customerId());
        assertEquals(startDate, target.startDate());
        assertTrue(target.rentedFilms().isEmpty());
        assertEquals(0, target.totalPrice());
    }

    @Test
    public void testRentFilm_one() {
        // Setup
        target = createAnonymousRental();
        Film film = createAnonymousFilm(FilmType.NEW);

        // Exercise
        target.rentFilm(film, 2);

        // Verify
        assertEquals(1, target.rentedFilms().size());
        assertEquals(80, target.rentedFilms().get(0).price());
        assertEquals(80, target.totalPrice());
    }

    @Test
    public void testRentFilm_two() {
        // Setup
        testRentFilm_one();
        Film film = createAnonymousFilm(FilmType.REGULAR);

        // Exercise
        target.rentFilm(film, 5);

        // Verify
        assertEquals(2, target.rentedFilms().size());
        assertEquals(90, target.rentedFilms().get(1).price());
        assertEquals(170, target.totalPrice());
    }

    @Test
    public void testRentFilm_three() {
        // Setup
        testRentFilm_two();
        Film film = createAnonymousFilm(FilmType.OLD);

        // Exercise
        target.rentFilm(film, 6);

        // Verify
        assertEquals(3, target.rentedFilms().size());
        assertEquals(60, target.rentedFilms().get(2).price());
        assertEquals(230, target.totalPrice());
    }

    @Test
    public void testReturnFilm_inTime() {
        // Setup
        Film film = createAnonymousFilm(FilmType.NEW);

        int nbOfDays = 3;

        LocalDate startDate = LocalDate.now();
        LocalDate returnDate = startDate.plusDays(nbOfDays);

        target = createAnonymousRental(startDate);
        target.rentFilm(film, nbOfDays);

        // Exercise
        target.returnFilm(film, returnDate);

        // Verify
        RentedFilm rentedFilm = target.rentedFilmOf(film.filmId());

        assertTrue(rentedFilm.returnDate().isPresent());
        assertEquals(returnDate, rentedFilm.returnDate().get());
        assertEquals(0, rentedFilm.lateCharge());
        assertEquals(0, target.totalLateCharge());
    }

    @Test
    public void testReturnFilm_lateReturn() {
        // Setup
        FilmType type = FilmType.REGULAR;
        Film film = createAnonymousFilm(FilmType.REGULAR);

        int nbOfDays = 5;
        int nbOfExtraDays = 1;
        int lateCharge = type.price() * nbOfExtraDays;

        LocalDate startDate = LocalDate.now();
        LocalDate returnDate = startDate.plusDays(nbOfDays + nbOfExtraDays);

        target = createAnonymousRental(startDate);
        target.rentFilm(film, nbOfDays);

        // Exercise
        target.returnFilm(film, returnDate);

        // Verify
        RentedFilm rentedFilm = target.rentedFilmOf(film.filmId());

        assertTrue(rentedFilm.returnDate().isPresent());
        assertEquals(returnDate, rentedFilm.returnDate().get());
        assertEquals(lateCharge, rentedFilm.lateCharge());
        assertEquals(lateCharge, target.totalLateCharge());
    }

    @Test
    public void testReturnFilm_additionalLateReturn() {
        // Setup
        testReturnFilm_lateReturn();

        int oldTotalLateCharge = target.totalLateCharge();

        FilmType type = FilmType.OLD;
        Film film = createAnonymousFilm(type);

        int nbOfDays = 10;
        int nbOfExtraDays = 2;
        int lateCharge = type.price() * nbOfExtraDays;

        LocalDate startDate = LocalDate.now();
        LocalDate returnDate = startDate.plusDays(nbOfDays + nbOfExtraDays);

        target.rentFilm(film, nbOfDays);

        // Exercise
        target.returnFilm(film, returnDate);

        // Verify
        RentedFilm rentedFilm = target.rentedFilmOf(film.filmId());

        assertTrue(rentedFilm.returnDate().isPresent());
        assertEquals(returnDate, rentedFilm.returnDate().get());
        assertEquals(lateCharge, rentedFilm.lateCharge());
        assertEquals(oldTotalLateCharge + lateCharge, target.totalLateCharge());
    }
}
