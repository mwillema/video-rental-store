package com.marcowillemart.videorentalstore.domain.rental;

import static com.marcowillemart.common.test.Tests.*;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import java.time.LocalDate;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class RentedFilmTest {

    private RentedFilm target;

    private FilmId filmId;
    private LocalDate dueDate;
    private int price;

    @Before
    public void setUp() {
        filmId = FilmId.newFilmId();
        dueDate = LocalDate.now().plusDays(1);
        price = createPositiveRandomNumber();

        target = new RentedFilm(filmId, dueDate, price);
    }

    @Test
    public void testFilmId() {
        // Exercise & Verify
        assertEquals(filmId, target.filmId());
    }

    @Test
    public void testDueDate() {
        // Exercise & Verify
        assertEquals(dueDate, target.dueDate());
    }

    @Test
    public void testPrice() {
        // Exercise & Verify
        assertEquals(price, target.price());
    }

    @Test
    public void testReturnDate() {
        // Exercise & Verify
        assertFalse(target.returnDate().isPresent());
    }

    @Test
    public void testLateCharge() {
        // Exercise & Verify
        assertEquals(0, target.lateCharge());
    }

    @Test
    public void testReturnFilm_inTime() {
        // Setup
        LocalDate returnDate = dueDate.minusDays(1);
        int lateCharge = 0;

        // Exercise
        target.returnFilm(returnDate, lateCharge);

        // Verify
        assertTrue(target.returnDate().isPresent());
        assertEquals(returnDate, target.returnDate().get());
        assertEquals(lateCharge, target.lateCharge());
    }

    @Test
    public void testReturnFilm_lateReturn() {
        // Setup
        LocalDate returnDate = dueDate.plusDays(1);
        int lateCharge = createPositiveRandomNumber();

        // Exercise
        target.returnFilm(returnDate, lateCharge);

        // Verify
        assertTrue(target.returnDate().isPresent());
        assertEquals(returnDate, target.returnDate().get());
        assertEquals(lateCharge, target.lateCharge());
    }
}
