package com.marcowillemart.videorentalstore.domain.customer;

import static com.marcowillemart.common.test.Tests.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    private Customer target;

    @Before
    public void setUp() {
        target =
                new Customer(
                        CustomerId.newCustomerId(),
                        createAnonymousName());
    }

    @Test
    public void testCreateCustomer() {
        // Setup
        CustomerId customerId = CustomerId.newCustomerId();
        String name = createAnonymousName();

        // Exercise
        target = new Customer(customerId, name);

        // Verify
        assertEquals(customerId, target.customerId());
    }

    @Test
    public void testGrantBonusPoints() {
        // Setup
        int bonusPoints = createPositiveRandomNumber();

        // Exercise
        target.grant(bonusPoints);

        // Verify
        assertEquals(bonusPoints, target.bonusPoints());
    }

    @Test
    public void testGrantBonusPoints_someMore() {
        // Setup
        target.grant(3);

        // Exercise
        target.grant(2);

        // Verify
        assertEquals(5, target.bonusPoints());
    }
}
