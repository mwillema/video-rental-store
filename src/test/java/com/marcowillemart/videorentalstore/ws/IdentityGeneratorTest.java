package com.marcowillemart.videorentalstore.ws;

import static org.junit.Assert.*;
import org.junit.Test;

public class IdentityGeneratorTest {

    @Test
    public void testNewIdentityFor_customer() {
        // Exercise
        String id = IdentityGenerator.newIdentityFor("customer");

        // Verify
        assertEquals(29, id.length());
        assertTrue(id.startsWith("VRS-C-"));
    }

    @Test
    public void testNewIdentityFor_film() {
        // Exercise
        String id = IdentityGenerator.newIdentityFor("film");

        // Verify
        assertEquals(29, id.length());
        assertTrue(id.startsWith("VRS-F-"));
    }

    @Test
    public void testNewIdentityFor_rental() {
        // Exercise
        String id = IdentityGenerator.newIdentityFor("rental");

        // Verify
        assertEquals(29, id.length());
        assertTrue(id.startsWith("VRS-R-"));
    }
}
