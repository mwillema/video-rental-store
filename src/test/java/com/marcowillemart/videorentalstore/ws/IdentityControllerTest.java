package com.marcowillemart.videorentalstore.ws;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class IdentityControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new IdentityController())
                .setMessageConverters(new StringHttpMessageConverter())
                .build();
    }

    @Test
    public void testNewIdentity_customer() throws Exception {
        // Exercise & Verify
        mockMvc.perform(post("/identities/customer"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testNewIdentity_film() throws Exception {
        // Exercise & Verify
        mockMvc.perform(post("/identities/film"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testNewIdentity_rental() throws Exception {
        // Exercise & Verify
        mockMvc.perform(post("/identities/rental"))
                .andExpect(status().isOk())
                .andDo(print());
    }
}
