package com.marcowillemart.videorentalstore.application;

import static com.marcowillemart.common.test.Tests.*;
import com.marcowillemart.videorentalstore.domain.customer.BonusPointsGranted;
import com.marcowillemart.videorentalstore.domain.customer.CustomerCreated;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.test.AbstractIntegrationTest;
import org.junit.Test;

public class CustomerApplicationServiceIT extends AbstractIntegrationTest {

    @Test
    public void testCreateCustomer() {
        // Setup
        String customerId = CustomerId.newCustomerId().id();
        String name = createAnonymousName();

        // Exercise
        process(CreateCustomer.newBuilder()
                .setCustomerId(customerId)
                .setName(name)
                .build());

        // Verify
        assertPublished(
                CustomerCreated.newBuilder()
                        .setCustomerId(customerId)
                        .setName(name)
                        .setInitialBonusPoints(0)
                        .build());
    }

    @Test
    public void testGrantBonusPoints() {
        // Setup
        String customerId = createCustomer();
        int bonusPoints = createPositiveRandomNumber();

        // Exercise
        process(GrantBonusPoints.newBuilder()
                .setCustomerId(customerId)
                .setBonusPoints(bonusPoints)
                .build());

        // Verify
        assertPublished(
                BonusPointsGranted.newBuilder()
                        .setCustomerId(customerId)
                        .setGrantedBonusPoints(bonusPoints)
                        .setTotalBonusPoints(bonusPoints)
                        .build());
    }

    @Test
    public void testGrantBonusPoints_grantSomeMore() {
        // Setup
        String customerId = createCustomer();
        int bonusPoints1 = createPositiveRandomNumber();
        grantBonusPoints(customerId, bonusPoints1);
        int bonusPoints2 = createPositiveRandomNumber();

        // Exercise
        process(GrantBonusPoints.newBuilder()
                .setCustomerId(customerId)
                .setBonusPoints(bonusPoints2)
                .build());

        // Verify
        assertPublished(
                BonusPointsGranted.newBuilder()
                        .setCustomerId(customerId)
                        .setGrantedBonusPoints(bonusPoints2)
                        .setTotalBonusPoints(bonusPoints1 + bonusPoints2)
                        .build());
    }
}
