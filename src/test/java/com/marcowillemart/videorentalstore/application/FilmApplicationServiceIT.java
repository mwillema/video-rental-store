package com.marcowillemart.videorentalstore.application;

import static com.marcowillemart.common.test.Tests.*;
import com.marcowillemart.videorentalstore.domain.film.FilmAdded;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import com.marcowillemart.videorentalstore.domain.film.FilmType;
import com.marcowillemart.videorentalstore.test.AbstractIntegrationTest;
import org.junit.Test;

public class FilmApplicationServiceIT extends AbstractIntegrationTest {

    @Test
    public void testAddFilm() {
        // Setup
        String filmId = FilmId.newFilmId().id();
        String title = createAnonymousName();
        String type = FilmType.REGULAR.name();

        // Exercise
        process(AddFilm.newBuilder()
                .setFilmId(filmId)
                .setTitle(title)
                .setType(type)
                .build());

        // Verify
        assertPublished(
                FilmAdded.newBuilder()
                        .setFilmId(filmId)
                        .setTitle(title)
                        .setType(FilmAdded.Type.valueOf(type))
                        .build());
    }
}
