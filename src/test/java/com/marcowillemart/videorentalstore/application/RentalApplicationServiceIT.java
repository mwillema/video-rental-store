package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.common.util.Timestamps;
import com.marcowillemart.videorentalstore.domain.customer.BonusPointsGranted;
import com.marcowillemart.videorentalstore.domain.film.FilmType;
import com.marcowillemart.videorentalstore.domain.rental.FilmRented;
import com.marcowillemart.videorentalstore.domain.rental.FilmReturned;
import com.marcowillemart.videorentalstore.domain.rental.RentalId;
import com.marcowillemart.videorentalstore.domain.rental.RentalStarted;
import com.marcowillemart.videorentalstore.test.AbstractIntegrationTest;
import java.time.LocalDate;
import org.junit.Test;

public class RentalApplicationServiceIT extends AbstractIntegrationTest {

    @Test
    public void testStartRental() {
        // Setup
        String rentalId = RentalId.newRentalId().id();
        String customerId = createCustomer();

        // Exercise
        process(StartRental.newBuilder()
                .setRentalId(rentalId)
                .setCustomerId(customerId)
                .build());

        // Verify
        assertPublished(
                RentalStarted.newBuilder()
                        .setRentalId(rentalId)
                        .setCustomerId(customerId)
                        .setStartDate(Timestamps.toTimeStamp(LocalDate.now()))
                        .build());
    }

    @Test
    public void testRentFilm() {
        // Setup
        FilmType type = FilmType.REGULAR;
        String filmId = addFilm(type);

        String customerId = createCustomer();

        String rentalId = startRental(customerId);
        int nbOfDays = 5;
        int price = type.priceFor(nbOfDays);
        int bonusPoints = type.bonusPoints();

        // Exercise
        process(RentFilm.newBuilder()
                .setRentalId(rentalId)
                .setFilmId(filmId)
                .setNbOfDays(nbOfDays)
                .build());

        // Verify
        assertPublished(
                FilmRented.newBuilder()
                        .setRentalId(rentalId)
                        .setCustomerId(customerId)
                        .setFilmId(filmId)
                        .setDueDate(Timestamps.toTimeStamp(
                                LocalDate.now().plusDays(nbOfDays)))
                        .setDays(nbOfDays)
                        .setFilmPrice(price)
                        .setTotalPrice(price)
                        .setFilmBonusPoints(bonusPoints)
                        .build());

        assertPublished(
                BonusPointsGranted.newBuilder()
                        .setCustomerId(customerId)
                        .setGrantedBonusPoints(bonusPoints)
                        .setTotalBonusPoints(bonusPoints)
                        .build());
    }

    @Test
    public void testReturnFilm() {
        // Setup
        FilmType type = FilmType.NEW;
        String filmId = addFilm(type);

        String customerId = createCustomer();

        String rentalId = startRental(customerId);

        int nbOfDays = 5;
        int nbOfExtraDays = 2;
        int lateCharge = nbOfExtraDays * type.price();

        rentFilm(rentalId, filmId, nbOfDays);

        LocalDate returnDate =
                LocalDate.now()
                        .plusDays(nbOfDays)
                        .plusDays(nbOfExtraDays);

        // Exercise
        process(ReturnFilm.newBuilder()
                .setRentalId(rentalId)
                .setFilmId(filmId)
                .setReturnDate(returnDate.toString())
                .build());

        // Verify
        assertPublished(
                FilmReturned.newBuilder()
                        .setRentalId(rentalId)
                        .setCustomerId(customerId)
                        .setFilmId(filmId)
                        .setReturnDate(Timestamps.toTimeStamp(returnDate))
                        .setExtraDays(nbOfExtraDays)
                        .setFilmLateCharge(lateCharge)
                        .setTotalLateCharge(lateCharge)
                        .build());
    }
}
