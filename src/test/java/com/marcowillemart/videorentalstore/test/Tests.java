package com.marcowillemart.videorentalstore.test;

import static com.marcowillemart.common.test.Tests.*;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.videorentalstore.domain.customer.Customer;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.domain.film.Film;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import com.marcowillemart.videorentalstore.domain.film.FilmType;
import com.marcowillemart.videorentalstore.domain.rental.Rental;
import com.marcowillemart.videorentalstore.domain.rental.RentalId;
import java.time.LocalDate;

public final class Tests {

    private Tests() {
        Assert.shouldNeverGetHere();
    }

    ////////////////////
    // CREATION METHODS
    ////////////////////

    public static Customer createAnonymousCustomer() {
        return new Customer(CustomerId.newCustomerId(), createAnonymousName());
    }

    public static Film createAnonymousFilm(FilmType type) {
        return new Film(FilmId.newFilmId(), createAnonymousName(), type);
    }

    public static Rental createAnonymousRental() {
        return createAnonymousRental(LocalDate.now());
    }

    public static Rental createAnonymousRental(LocalDate startDate) {
        return new Rental(
                RentalId.newRentalId(),
                createAnonymousCustomer(),
                () -> startDate);
    }

    ////////////////////
    // UTILITY METHODS
    ////////////////////

    ////////////////////
    // CUSTOM ASSERTIONS
    ////////////////////
}
