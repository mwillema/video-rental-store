package com.marcowillemart.videorentalstore.test;

import com.marcowillemart.common.test.DomainEventCatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public DomainEventCatcher domainEventCatcher() {
        return new DomainEventCatcher();
    }
}
