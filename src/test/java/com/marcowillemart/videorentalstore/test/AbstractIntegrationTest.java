package com.marcowillemart.videorentalstore.test;

import com.google.protobuf.Message;
import com.marcowillemart.common.test.DomainEventCatcher;
import com.marcowillemart.common.test.EventMatcher;
import static com.marcowillemart.common.test.Tests.*;
import com.marcowillemart.videorentalstore.application.AddFilm;
import com.marcowillemart.videorentalstore.application.CommandProcessor;
import com.marcowillemart.videorentalstore.application.CreateCustomer;
import com.marcowillemart.videorentalstore.application.GrantBonusPoints;
import com.marcowillemart.videorentalstore.application.RentFilm;
import com.marcowillemart.videorentalstore.application.StartRental;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import com.marcowillemart.videorentalstore.domain.film.FilmType;
import com.marcowillemart.videorentalstore.domain.rental.RentalId;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public abstract class AbstractIntegrationTest {

    @Autowired
    private CommandProcessor processor;

    @Autowired
    private DomainEventCatcher eventCatcher;

    protected AbstractIntegrationTest() {
    }

    @Before
    public void setUp() {
        eventCatcher.clear();
    }

    protected void process(Message command) {
        processor.process(command);
    }

    protected void assertPublished(Message event) {
        eventCatcher.assertPublished(event);
    }

    protected void assertPublished(Class<? extends Message> eventType) {
        eventCatcher.assertPublished(eventType);
    }

    protected void assertPublished(
            Class<? extends Message> eventType,
            EventMatcher... matchers) {

        eventCatcher.assertPublished(eventType, matchers);
    }

    ////////////////////
    // FILM
    ////////////////////

    protected String addFilm(FilmType type) {
        String filmId = FilmId.newFilmId().id();

        process(AddFilm.newBuilder()
                .setFilmId(filmId)
                .setTitle(createAnonymousName())
                .setType(type.toString())
                .build());

        eventCatcher.clear();
        return filmId;
    }

    ////////////////////
    // CUSTOMER
    ////////////////////

    protected String createCustomer() {
        String customerId = CustomerId.newCustomerId().id();

        process(CreateCustomer.newBuilder()
                .setCustomerId(customerId)
                .setName(createAnonymousName())
                .build());

        eventCatcher.clear();

        return customerId;
    }

    protected void grantBonusPoints(String customerId, int bonusPoints) {
        process(GrantBonusPoints.newBuilder()
                .setCustomerId(customerId)
                .setBonusPoints(bonusPoints)
                .build());

        eventCatcher.clear();
    }

    ////////////////////
    // RENTAL
    ////////////////////

    protected String startRental(String customerId) {
        String rentalId = RentalId.newRentalId().id();

        process(StartRental.newBuilder()
                .setRentalId(rentalId)
                .setCustomerId(customerId)
                .build());

        eventCatcher.clear();
        return rentalId;
    }

    protected void rentFilm(String rentalId, String filmId, int nbOfDays) {
        process(RentFilm.newBuilder()
                .setRentalId(rentalId)
                .setFilmId(filmId)
                .setNbOfDays(nbOfDays)
                .build());

        eventCatcher.clear();
    }
}
