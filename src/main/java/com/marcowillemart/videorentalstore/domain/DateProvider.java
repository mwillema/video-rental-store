package com.marcowillemart.videorentalstore.domain;

import java.time.LocalDate;

public interface DateProvider {

    LocalDate today();
}
