package com.marcowillemart.videorentalstore.domain.film;

import com.marcowillemart.common.util.Assert;

/**
 * FilmType is a value object representing an immutable type of film.
 *
 * @specfield price : Price         // The base price for a rental of this type
 *                                     of film.
 * @specfield bonusPoints : integer // The number of bonus point a rental of
 *                                     this type of film grants.
 *
 * @invariant bonusPoints > 0
 */
public enum FilmType {

    /** New releases. */
    NEW(Price.PREMIUM, 2) {

        @Override
        public int priceFor(int days) {
            return price() * days;
        }
    },

    /** Regular films. */
    REGULAR(Price.BASIC, 1) {

        @Override
        public int priceFor(int days) {
            int remainingDays = days > 3 ? days - 3 : 0;
            return price() + price() * remainingDays;
        }
    },

    /** Old films. */
    OLD(Price.BASIC, 1) {

        @Override
        public int priceFor(int days) {
            int remainingDays = days > 5 ? days - 5 : 0;
            return price() + price() * remainingDays;
        }
    };

    private final Price price;
    private final int bonusPoints;

    private FilmType(Price price, int bonusPoints) {
        Assert.notNull(price);
        Assert.isTrue(bonusPoints > 0);

        this.price = price;
        this.bonusPoints = bonusPoints;
    }

    /**
     * @requires days > 0
     * @return the price corresponding to this film type for the given number
     *         of days
     */
    public abstract int priceFor(int days);

    /**
     * @return this.price.value
     */
    public int price() {
        return price.value();
    }

    /**
     * @return this.bonusPoints
     */
    public int bonusPoints() {
        return bonusPoints;
    }
}
