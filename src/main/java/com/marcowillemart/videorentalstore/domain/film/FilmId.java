package com.marcowillemart.videorentalstore.domain.film;

import com.marcowillemart.common.domain.IdentityGenerator;
import com.marcowillemart.common.domain.StructuredId;

/**
 * FilmId is a value object representing the immutable identity of a film.
 */
public final class FilmId extends StructuredId {

    private static final String KEY = "VRS";
    private static final char CODE = 'F';

    /**
     * @requires id != null && ID(id)
     * @effects Makes this be a new FilmId i with i.id = id
     */
    public FilmId(String id) {
        super(id);
    }

    /**
     * @return a new FilmId
     */
    public static FilmId newFilmId() {
        return new FilmId(
                IdentityGenerator.defaultInstance().newIdentity(KEY, CODE));
    }
}
