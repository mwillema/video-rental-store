package com.marcowillemart.videorentalstore.domain.film;

import com.google.protobuf.Message;
import com.marcowillemart.common.domain.EventSourcedAggregate;
import com.marcowillemart.common.util.Assert;

/**
 * Film is a root entity representing a mutable film that can be rented by
 * customers.
 *
 * @specfield filmId : FilmId // The unique identity of the film.
 * @specfield title : string  // The title of the film.
 * @specfield type : FilmType // The type of the film.
 *
 * @invariant title not empty
 */
public final class Film extends EventSourcedAggregate {

    private FilmId filmId;
    private String title;
    private FilmType type;

    /**
     * @effects Asserts the rep invariant holds for this
     */
    private void checkRep() {
        Assert.notNull(filmId);
        Assert.notEmpty(title);
        Assert.notNull(type);
    }

    /**
     * @requires identity not null and not empty &&
     *           version > 0 &&
     *           events not null and not empty && no null element in events
     * @effects Makes this be a new Film f with
     *          f.identity = identity, f.version = version, f.changes = [] and
     *          rehydrated with the given events.
     */
    public Film(
            String identity,
            int version,
            Iterable<Message> events) {

        super(identity, version, events);
    }

    /**
     * @requires filmId != null
     * @effects Makes this be a new Film f with c.identity = filmId,
     *          f.version = 0, f.changes = [], f.filmId = filmId,
     *          f.title = title and f.type = type
     */
    public Film(FilmId filmId, String title, FilmType type) {
        super(filmId);

        apply(FilmAdded.newBuilder()
                .setFilmId(filmId.id())
                .setTitle(title)
                .setType(FilmAdded.Type.valueOf(type.name()))
                .build());

        checkRep();
    }

    /**
     * @return this.filmId
     */
    public FilmId filmId() {
        return filmId;
    }

    /**
     * @return this.title
     */
    public String title() {
        return title;
    }

    /**
     * @return this.type
     */
    public FilmType type() {
        return type;
    }

    /**
     * @requires nbOfDays > 0
     * @return the rental price of this film for the given number of days
     */
    public int priceFor(int nbOfDays) {
        Assert.isTrue(nbOfDays > 0);

        return type.priceFor(nbOfDays);
    }

    /**
     * @return the number of bonus points this film grants to the customer
     *         renting it
     */
    public int bonusPoints() {
        return type.bonusPoints();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void when(FilmAdded event) {
        filmId = new FilmId(event.getFilmId());
        title = event.getTitle();
        type = FilmType.valueOf(event.getType().name());
    }
}
