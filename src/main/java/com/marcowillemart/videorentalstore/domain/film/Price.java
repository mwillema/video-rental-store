package com.marcowillemart.videorentalstore.domain.film;

import com.marcowillemart.common.util.Assert;

/**
 * Price is a value object representing an immutable price in SEK.
 *
 * @specfield value : integer // The value of the price in SEK.
 */
public enum Price {

    /** Premium price. */
    PREMIUM(40),
    
    /** Basic price. */
    BASIC(30);

    private final int value;

    private Price(int value) {
        Assert.isTrue(value > 0);

        this.value = value;
    }

    /**
     * @return this.value
     */
    public int value() {
        return value;
    }
}
