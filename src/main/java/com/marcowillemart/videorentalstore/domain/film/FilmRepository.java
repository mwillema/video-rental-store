package com.marcowillemart.videorentalstore.domain.film;

import com.marcowillemart.common.domain.Repository;

/**
 * FilmRepository represents a mutable repository of Films.
 */
public interface FilmRepository extends Repository<Film, FilmId> {
}
