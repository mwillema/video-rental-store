package com.marcowillemart.videorentalstore.domain.rental;

import com.marcowillemart.common.util.Assert;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import java.time.LocalDate;
import java.util.Optional;

/**
 * RentedFilm is an entity representing a mutable film that is rented by a
 * customer.
 *
 * @specfield filmId : FilmId         // The unique identity of the rented film.
 * @specfield dueDate : date          // The due date of the rented film.
 * @specfield price : int             // The price in SEK of the rented film.
 * @specfield returnDate : date [0-1] // The return date of the rented film, if
 *                                       already returned.
 * @specfield lateCharge : integer    // The late charge in SEK of the rented
 *                                       film.
 *
 * @invariant price > 0
 * @invariant lateCharge >= 0
 * @invariant lateCharge > 0 -> returnDate != nil
 * @invariant returnDate = nil -> lateCharge = 0
 */
public final class RentedFilm {

    private final FilmId filmId;
    private final LocalDate dueDate;
    private final int price;
    private Optional<LocalDate> returnDate;
    private int lateCharge;

    /**
     * @effects Asserts the rep invariant holds for this
     */
    private void checkRep() {
        Assert.notNull(filmId);
        Assert.notNull(dueDate);
        Assert.isTrue(price > 0);
        Assert.notNull(returnDate);
        Assert.isTrue(lateCharge >= 0);
        Assert.implies(lateCharge > 0, returnDate.isPresent());
        Assert.implies(!returnDate.isPresent(), lateCharge == 0);
    }

    /**
     * @requires filmId != null && dueDate != null && dueDate in the future &&
     *           price > 0
     * @effects Makes this be a new RentedFilm rf with rf.filmId = filmId,
     *          rf.dueDate = dueDate, rf.price = price, rf.returnDate = nil and
     *          rf.lateCharge = 0
     */
    RentedFilm(FilmId filmId, LocalDate dueDate, int price) {
        Assert.notNull(filmId);
        Assert.notNull(dueDate);
        Assert.isTrue(dueDate.isAfter(LocalDate.now()));
        Assert.isTrue(price > 0);

        this.filmId = filmId;
        this.dueDate = dueDate;
        this.price = price;
        this.returnDate = Optional.empty();
        this.lateCharge = 0;

        checkRep();
    }

    /**
     * @return this.filmId
     */
    public FilmId filmId() {
        return filmId;
    }

    /**
     * @return this.dueDate
     */
    public LocalDate dueDate() {
        return dueDate;
    }

    /**
     * @return this.price
     */
    public int price() {
        return price;
    }

    /**
     * @return this.returnDate
     */
    public Optional<LocalDate> returnDate() {
        return returnDate;
    }

    /**
     * @return this.lateCharge
     */
    public int lateCharge() {
        return lateCharge;
    }

    /**
     * @requires this.returnDate is nil && returnDate != null && lateCharge >= 0
     * @modifies this
     * @effects Returns this rented film, i.e., sets
     *          this.returnDate = returnDate and this.lateCharge = lateCharge
     */
    void returnFilm(LocalDate returnDate, int lateCharge) {
        Assert.isFalse(this.returnDate.isPresent());
        Assert.notNull(returnDate);
        Assert.isTrue(lateCharge >= 0);

        this.returnDate = Optional.of(returnDate);
        this.lateCharge = lateCharge;

        checkRep();
    }
}
