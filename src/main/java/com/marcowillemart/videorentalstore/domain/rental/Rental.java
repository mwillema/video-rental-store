package com.marcowillemart.videorentalstore.domain.rental;

import com.google.protobuf.Message;
import com.marcowillemart.common.domain.EventSourcedAggregate;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.FailureException;
import com.marcowillemart.common.util.Filterer;
import com.marcowillemart.common.util.Timestamps;
import com.marcowillemart.videorentalstore.domain.DateProvider;
import com.marcowillemart.videorentalstore.domain.customer.Customer;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.domain.film.Film;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Rental is a root entity representing a mutable rental of films by a customer.
 *
 * @specfield rentalId: RentalId           // The unique identity of the rental.
 * @specfield customerId : CustomerId      // The unique identity of the
 *                                            customer of the rental.
 * @specfield startDate : date             // The date of the start of the
 *                                            rental.
 * @specfield rentedFilms : RentedFilm[*]  // The rented films.
 *
 * @derivedfield totalPrice : integer      // The total price in SEK of all the
 *                                            rented films.
 *                                         // SUM(rf.price | rf in rentedFilms)
 * @derivedfield totalLateCharge : integer // The total of late charges in SEK
 *                                            of all the rented films.
 *                                            SUM(rf.lateCharge
 *                                                  | rf in rentedFilms)
 *
 * @invariant startDate not in the future
 */
public final class Rental extends EventSourcedAggregate {

    private RentalId rentalId;
    private CustomerId customerId;
    private LocalDate startDate;
    private List<RentedFilm> rentedFilms;

    /**
     * @effects Asserts the rep invariant holds for this
     */
    private void checkRep() {
        Assert.notNull(rentalId);
        Assert.notNull(customerId);
        Assert.notNull(startDate);
        LocalDate now = LocalDate.now();
        Assert.isFalse(startDate.isAfter(now));
        Assert.notNull(rentedFilms);
    }

    /**
     * @requires identity not null and not empty &&
     *           version > 0 &&
     *           events not null and not empty && no null element in events
     * @effects Makes this be a new Rental r with r.identity = identity,
     *          r.version = version, r.changes = [] and rehydrated with the
     *          given events.
     */
    public Rental(
            String identity,
            int version,
            Iterable<Message> events) {

        super(identity, version, events);
    }

    /**
     * @requires all params are not null
     * @effects Makes this be a new Rental r with r.identity = rentalId,
     *          r.version = 0, r.changes = [], r.rentalId = rentalId,
     *          r.customer = customer.customerId and
     *          r.startDate = dateProvider.today()
     */
    public Rental(
            RentalId rentalId,
            Customer customer,
            DateProvider dateProvider) {

        super(rentalId);

        Assert.notNull(customer);
        Assert.notNull(dateProvider);

        apply(RentalStarted.newBuilder()
                .setRentalId(rentalId.id())
                .setCustomerId(customer.customerId().id())
                .setStartDate(Timestamps.toTimeStamp(dateProvider.today()))
                .build());

        checkRep();
    }

    /**
     * @requires film != null && nbOfDays > 0 && this.startDate = TODAY
     * @modifies this
     * @effects Adds film to this.rentedFilms for the given number of days.
     */
    public void rentFilm(Film film, int nbOfDays) {
        Assert.notNull(film);
        Assert.isTrue(nbOfDays > 0);
        Assert.equals(LocalDate.now(), startDate);

        int filmPrice = film.priceFor(nbOfDays);

        apply(FilmRented.newBuilder()
                .setRentalId(rentalId.id())
                .setCustomerId(customerId.id())
                .setFilmId(film.filmId().id())
                .setDueDate(
                        Timestamps.toTimeStamp(startDate.plusDays(nbOfDays)))
                .setDays(nbOfDays)
                .setFilmPrice(filmPrice)
                .setTotalPrice(totalPrice() + filmPrice)
                .setFilmBonusPoints(film.bonusPoints())
                .build());

        checkRep();
    }

    /**
     * @requires film != null && there exists rf in this.rentedFilms such that
     *           rf.filmId = film.filmId and rf.returnDate is nil &&
     *           returnDate != null
     * @modifies this
     * @effects Returns the given film and computes the possible late charges,
     *          i.e., sets rf.returnDate to returnDate and sets rf.lateCharge to
     *          the computed late charges where rf in this.rentedFilms with
     *          rf.filmId = film.filmId
     */
    public void returnFilm(Film film, LocalDate returnDate) {
        Assert.notNull(film);
        Assert.notNull(returnDate);

        RentedFilm rentedFilm = notReturnedRentedFilmOf(film.filmId());
        int extraDays = 0;
        int lateCharge = 0;

        if (returnDate.isAfter(rentedFilm.dueDate())) {
            extraDays =
                    Period.between(rentedFilm.dueDate(), returnDate).getDays();

            int totalDays = Period.between(startDate, returnDate).getDays();
            lateCharge = film.priceFor(totalDays) - rentedFilm.price();
        }

        apply(FilmReturned.newBuilder()
                .setRentalId(rentalId.id())
                .setCustomerId(customerId.id())
                .setFilmId(rentedFilm.filmId().id())
                .setReturnDate(Timestamps.toTimeStamp(returnDate))
                .setExtraDays(extraDays)
                .setFilmLateCharge(lateCharge)
                .setTotalLateCharge(totalLateCharge() + lateCharge)
                .build());

        checkRep();
    }

    /**
     * @return this.rentalId
     */
    public RentalId rentalId() {
        return rentalId;
    }

    /**
     * @return this.customerId
     */
    public CustomerId customerId() {
        return customerId;
    }

    /**
     * @return this.startDate
     */
    public LocalDate startDate() {
        return startDate;
    }

    /**
     * @requires filmId != null &&
     *           there exists rf in this.rentedFilms with rf.filmId = filmId
     * @return the first rf in this.rentedFilms with rf.filmId = filmId
     */
    public RentedFilm rentedFilmOf(FilmId filmId) {
        return rentedFilmOf(filmId, rentedFilm -> true);
    }

    /**
     * @return this.rentedFilms
     */
    public List<RentedFilm> rentedFilms() {
        return Collections.unmodifiableList(rentedFilms);
    }

    /**
     * @return this.totalPrice
     */
    public int totalPrice() {
        return rentedFilms
                .stream()
                .mapToInt(RentedFilm::price)
                .sum();
    }

    /**
     * @return this.totalLateCharge
     */
    public int totalLateCharge() {
        return rentedFilms
                .stream()
                .mapToInt(RentedFilm::lateCharge)
                .sum();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void when(RentalStarted event) {
        rentalId = new RentalId(event.getRentalId());
        customerId = new CustomerId(event.getCustomerId());
        startDate = Timestamps.toLocalDate(event.getStartDate());
        rentedFilms = new LinkedList<>();
    }

    private void when(FilmRented event) {
        rentedFilms.add(
                new RentedFilm(
                        new FilmId(event.getFilmId()),
                        Timestamps.toLocalDate(event.getDueDate()),
                        event.getFilmPrice()));
    }

    private void when(FilmReturned event) {
        notReturnedRentedFilmOf(new FilmId(event.getFilmId()))
                .returnFilm(
                Timestamps.toLocalDate(event.getReturnDate()),
                        event.getFilmLateCharge());
    }

    private RentedFilm notReturnedRentedFilmOf(FilmId filmId) {
        return rentedFilmOf(filmId,
                rentedFilm -> !rentedFilm.returnDate().isPresent());
    }

    private RentedFilm rentedFilmOf(
            FilmId filmId,
            Filterer<RentedFilm> filter) {

        return rentedFilms
                .stream()
                .filter(rentedFilm ->
                        rentedFilm.filmId().equals(filmId)
                                && filter.check(rentedFilm))
                .findFirst()
                .orElseThrow(FailureException::new);
    }
}
