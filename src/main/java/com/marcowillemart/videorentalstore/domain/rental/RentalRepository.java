package com.marcowillemart.videorentalstore.domain.rental;

import com.marcowillemart.common.domain.Repository;

/**
 * RentalRepository represents a mutable repository of Rentals.
 */
public interface RentalRepository extends Repository<Rental, RentalId> {
}
