package com.marcowillemart.videorentalstore.domain.rental;

import com.marcowillemart.common.domain.IdentityGenerator;
import com.marcowillemart.common.domain.StructuredId;

/**
 * RentalId is a value object representing the immutable identity of a rental.
 */
public final class RentalId extends StructuredId {

    private static final String KEY = "VRS";
    private static final char CODE = 'R';

    /**
     * @requires id != null && ID(id)
     * @effects Makes this be a new RentalId i with i.id = id
     */
    public RentalId(String id) {
        super(id);
    }

    /**
     * @return a new RentalId
     */
    public static RentalId newRentalId() {
        return new RentalId(
                IdentityGenerator.defaultInstance().newIdentity(KEY, CODE));
    }
}
