package com.marcowillemart.videorentalstore.domain.customer;

import com.marcowillemart.common.domain.Repository;

/**
 * CustomerRepository represents a mutable repository of Customers.
 */
public interface CustomerRepository extends Repository<Customer, CustomerId> {
}
