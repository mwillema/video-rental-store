package com.marcowillemart.videorentalstore.domain.customer;

import com.marcowillemart.common.domain.IdentityGenerator;
import com.marcowillemart.common.domain.StructuredId;

/**
 * CustomerId is a value object representing the immutable identity of a
 * customer.
 */
public final class CustomerId extends StructuredId {

    private static final String KEY = "VRS";
    private static final char CODE = 'C';

    /**
     * @requires id != null && ID(id)
     * @effects Makes this be a new CustomerId i with i.id = id
     */
    public CustomerId(String id) {
        super(id);
    }

    /**
     * @return a new CustomerId
     */
    public static CustomerId newCustomerId() {
        return new CustomerId(
                IdentityGenerator.defaultInstance().newIdentity(KEY, CODE));
    }
}
