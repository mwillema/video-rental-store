package com.marcowillemart.videorentalstore.domain.customer;

import com.google.protobuf.Message;
import com.marcowillemart.common.domain.EventSourcedAggregate;
import com.marcowillemart.common.util.Assert;

/**
 * Customer is a root entity representing a mutable customer of the video rental
 * store who can rent films.
 *
 * @specfield customerId : CustomerId // The unique identity of the customer.
 * @specfield name : string           // The name of the customer.
 * @specfield bonusPoints : integer   // The number of bonus points of the
 *                                       customer.
 *
 * @invariant name.length > 0
 * @invariant bonusPoints >= 0
 */
public final class Customer extends EventSourcedAggregate {

    private static final int INITIAL_BONUS_POINTS = 0;

    private CustomerId customerId;
    private String name;
    private int bonusPoints;

    /**
     * @effects Asserts the rep invariant holds for this
     */
    private void checkRep() {
        Assert.notNull(customerId);
        Assert.notEmpty(name);
        Assert.isTrue(bonusPoints >= 0);
    }

    /**
     * @requires identity not null and not empty &&
     *           version > 0 &&
     *           events not null and not empty && no null element in events
     * @effects Makes this be a new Customer c with
     *          c.identity = identity, c.version = version, c.changes = [] and
     *          rehydrated with the given events.
     */
    public Customer(
            String identity,
            int version,
            Iterable<Message> events) {

        super(identity, version, events);
    }

    /**
     * @requires customerId != null && name not null and not empty
     * @effects Makes this be a new Customer c with c.identity = customerId,
     *          c.version = 0, c.changes = [], c.customerId = customerId,
     *          c.name = name and c.bonusPoints = 0
     */
    public Customer(CustomerId customerId, String name) {
        super(customerId);

        Assert.notEmpty(name);

        apply(CustomerCreated.newBuilder()
                .setCustomerId(customerId.id())
                .setName(name)
                .setInitialBonusPoints(INITIAL_BONUS_POINTS)
                .build());

        checkRep();
    }

    /**
     * @requires bonusPoints > 0
     * @modifies this
     * @effects Sets this.bonusPoints = this_pre.bonusPoints + bonusPoints
     */
    public void grant(int bonusPoints) {
        Assert.isTrue(bonusPoints > 0);

        apply(BonusPointsGranted.newBuilder()
                .setCustomerId(customerId.id())
                .setGrantedBonusPoints(bonusPoints)
                .setTotalBonusPoints(this.bonusPoints + bonusPoints)
                .build());

        checkRep();
    }

    /**
     * @return this.customerId
     */
    public CustomerId customerId() {
        return customerId;
    }

    /**
     * @return this.bonusPoints
     */
    public int bonusPoints() {
        return bonusPoints;
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void when(CustomerCreated event) {
        customerId = new CustomerId(event.getCustomerId());
        name = event.getName();
        bonusPoints = event.getInitialBonusPoints();
    }

    private void when(BonusPointsGranted event) {
        bonusPoints = event.getTotalBonusPoints();
    }
}
