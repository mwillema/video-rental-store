package com.marcowillemart.videorentalstore;

import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Descriptors.FileDescriptor;
import com.marcowillemart.common.util.MessageTypeRegistry;
import com.marcowillemart.videorentalstore.application.CommandProto;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class ApplicationConfig implements RepositoryRestConfigurer {

    @Autowired
    private MessageTypeRegistry registry;

    @Autowired
    private EntityManager entityManager;

    public ApplicationConfig() {
    }

    @EventListener(ContextRefreshedEvent.class)
    public void onStartup() {
        registerCommands();
    }

    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addExposedHeader("Content-Type");
        config.addExposedHeader("Authorization");
        config.addExposedHeader("Accept");
        config.addExposedHeader("Origin");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);

        return new CorsFilter(source);
    }

    @Override
    public void configureRepositoryRestConfiguration(
            RepositoryRestConfiguration config) {

        config.exposeIdsFor(
                entityManager.getMetamodel().getEntities()
                        .stream()
                        .map(EntityType::getJavaType)
                        .collect(Collectors.toList())
                        .toArray(new Class[0]));
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void registerCommands() {
        register(CommandProto.getDescriptor());
    }

    private void register(FileDescriptor fileDescriptor) {
        register(fileDescriptor.getMessageTypes());
    }

    private void register(List<Descriptor> descriptors) {
        descriptors.forEach(registry::register);
    }
}
