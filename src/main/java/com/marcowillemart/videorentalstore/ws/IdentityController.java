package com.marcowillemart.videorentalstore.ws;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/identities", produces = MediaType.TEXT_PLAIN_VALUE)
public class IdentityController {

    public IdentityController() {
    }

    @PostMapping("/{entityName}")
    public String newIdentity(@PathVariable String entityName) {
        return IdentityGenerator.newIdentityFor(entityName);
    }
}
