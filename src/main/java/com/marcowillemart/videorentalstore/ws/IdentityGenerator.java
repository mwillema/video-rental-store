package com.marcowillemart.videorentalstore.ws;

import com.marcowillemart.common.domain.Identity;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.FailureException;
import java.lang.reflect.Method;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class IdentityGenerator {

    private static final Pattern HYPHEN_PATTERN = Pattern.compile("\\-([a-z])");

    static String newIdentityFor(String entityName) {
        Assert.notEmpty(entityName);

        return callNewIdentity(
                classNameOf(entityName),
                methodNameOf(entityName));
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private static String classNameOf(String entityName) {
        return "com.marcowillemart.videorentalstore.domain."
                + replaceHyphens(entityName, str -> str)
                + "."
                + idTypeNameOf(entityName);
    }

    private static String methodNameOf(String entityName) {
        return "new" + idTypeNameOf(entityName);
    }

    private static String idTypeNameOf(String entityName) {
        return entityName.substring(0, 1).toUpperCase()
                + replaceHyphens(
                        entityName.substring(1),
                        str -> str.toUpperCase())
                + "Id";
    }

    private static String callNewIdentity(String className, String methodName) {
        try {
            Class<?> entityId = Class.forName(className);
            Method method = entityId.getMethod(methodName);
            Object newIdentity = method.invoke(null);
            Assert.isTrue(newIdentity instanceof Identity);
            return ((Identity) newIdentity).id();
        } catch (ReflectiveOperationException ex) {
            throw new FailureException(ex);
        }
    }

    private static String replaceHyphens(
            String str,
            Function<String, String> replacement) {

        Matcher matcher = HYPHEN_PATTERN.matcher(str);

        while (matcher.find()) {
            str = str.replaceFirst(
                    matcher.group(),
                    replacement.apply(matcher.group(1)));
        }

        return str;
    }
}
