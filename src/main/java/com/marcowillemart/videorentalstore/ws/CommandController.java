package com.marcowillemart.videorentalstore.ws;

import com.google.protobuf.Any;
import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.Messages;
import com.marcowillemart.videorentalstore.application.CommandProcessor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/commands")
public class CommandController {

    private static final String PACKAGE_NAME = "com.marcowillemart";

    private final CommandProcessor processor;

    public CommandController(CommandProcessor processor) {
        Assert.notNull(processor);

        this.processor = processor;
    }

    @PutMapping
    public void process(@RequestBody Any any) {
        Assert.notNull(any);

        processor.process(
                Messages.unpack(
                        any,
                        Messages.classForName(
                                PACKAGE_NAME,
                                Messages.typeNameOf(any.getTypeUrl()))));
    }
}
