package com.marcowillemart.videorentalstore;

import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.FollowStoreEventDispatcher;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfig {

    public DomainConfig() {
    }

    @Bean
    public FollowStoreEventDispatcher followStoreEventDispatcher(
            EventStore eventStore,
            ApplicationEventPublisher publisher) {

        return new FollowStoreEventDispatcher(eventStore, publisher);
    }
}
