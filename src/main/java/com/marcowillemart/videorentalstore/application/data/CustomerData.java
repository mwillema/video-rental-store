package com.marcowillemart.videorentalstore.application.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import lombok.Data;

@Data
@Entity(name = "customer")
public class CustomerData implements Serializable {

    @Id
    private String customerId;

    @NotEmpty
    private String name;

    @PositiveOrZero
    private int bonusPoints;

    @OneToMany(cascade = CascadeType.ALL)
    @MapKey(name = "rentalId")
    @OrderBy("rentalId")
    private Map<String, RentalData> rentals = new LinkedHashMap<>();
}
