package com.marcowillemart.videorentalstore.application.data;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.Data;

@Data
@Embeddable
public class RentedFilmData implements Serializable {

    @NotNull
    private String filmId;

    @Positive
    private int days;

    @Positive
    private int price;

    @PositiveOrZero
    private int extraDays = 0;

    @PositiveOrZero
    private int lateCharge = 0;

    @NotEmpty
    private String status;
}
