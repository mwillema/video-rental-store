package com.marcowillemart.videorentalstore.application.data;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity(name = "film")
public class FilmData implements Serializable {

    @Id
    private String filmId;

    @NotEmpty
    private String title;

    @NotEmpty
    private String type;
}
