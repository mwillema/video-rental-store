package com.marcowillemart.videorentalstore.application.data;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.validation.constraints.PositiveOrZero;
import lombok.Data;

@Data
@Entity(name = "rental")
public class RentalData implements Serializable {

    @Id
    private String rentalId;

    @PositiveOrZero
    private int totalPrice;

    @PositiveOrZero
    private int totalCharge;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<RentedFilmData> rentedFilms = new LinkedHashSet<>();
}
