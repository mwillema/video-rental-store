package com.marcowillemart.videorentalstore.application;

import com.google.protobuf.Message;
import com.marcowillemart.common.util.Assert;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class CommandProcessor {

    private final ApplicationEventPublisher publisher;

    public CommandProcessor(ApplicationEventPublisher publisher) {
        Assert.notNull(publisher);

        this.publisher = publisher;
    }

    public void process(Message command) {
        Assert.notNull(command);

        publisher.publishEvent(command);
    }
}
