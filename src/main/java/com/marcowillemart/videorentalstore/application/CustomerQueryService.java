package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.common.projection.QueryService;
import com.marcowillemart.videorentalstore.application.data.CustomerData;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "customers", path = "customers")
public interface CustomerQueryService extends QueryService<CustomerData> {
}
