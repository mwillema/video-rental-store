package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.common.projection.QueryService;
import com.marcowillemart.videorentalstore.application.data.FilmData;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "films", path = "films")
public interface FilmQueryService extends QueryService<FilmData> {
}
