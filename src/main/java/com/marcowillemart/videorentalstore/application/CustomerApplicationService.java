package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.videorentalstore.domain.customer.Customer;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.domain.customer.CustomerRepository;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class CustomerApplicationService {

    private static final Logger LOG =
            LoggerFactory.getLogger(CustomerApplicationService.class);

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerApplicationService() {
    }

    @EventListener
    public void execute(CreateCustomer command) {
        Customer customer =
                new Customer(
                        new CustomerId(
                                command.getCustomerId()),
                                command.getName());

        customerRepository.save(customer);

        LOG.info("Customer '{}' created [{}]",
                command.getName(),
                command.getCustomerId());
    }

    @EventListener
    public void execute(GrantBonusPoints command) {
        update(command.getCustomerId(),
                customer -> customer.grant(command.getBonusPoints()));

        LOG.info("{} bonus points granted [{}]",
                command.getBonusPoints(),
                command.getCustomerId());
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void update(
            String customerId,
            Consumer<Customer> execute) {

        Customer customer = customerRepository.find(new CustomerId(customerId));

        execute.accept(customer);

        customerRepository.save(customer);
    }
}
