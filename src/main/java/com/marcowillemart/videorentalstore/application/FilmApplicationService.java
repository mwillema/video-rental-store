package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.videorentalstore.domain.film.Film;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import com.marcowillemart.videorentalstore.domain.film.FilmRepository;
import com.marcowillemart.videorentalstore.domain.film.FilmType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class FilmApplicationService {

    private static final Logger LOG =
            LoggerFactory.getLogger(FilmApplicationService.class);

    @Autowired
    private FilmRepository filmRepository;

    public FilmApplicationService() {
    }

    @EventListener
    public void execute(AddFilm command) {
        Film film =
                new Film(
                        new FilmId(command.getFilmId()),
                        command.getTitle(),
                        FilmType.valueOf(command.getType()));

        filmRepository.save(film);

        LOG.info("Film '{}' added [{}]",
                command.getTitle(),
                command.getFilmId());
    }
}
