package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.videorentalstore.domain.DateProvider;
import com.marcowillemart.videorentalstore.domain.customer.Customer;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.domain.customer.CustomerRepository;
import com.marcowillemart.videorentalstore.domain.film.Film;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import com.marcowillemart.videorentalstore.domain.film.FilmRepository;
import com.marcowillemart.videorentalstore.domain.rental.Rental;
import com.marcowillemart.videorentalstore.domain.rental.RentalId;
import com.marcowillemart.videorentalstore.domain.rental.RentalRepository;
import java.time.LocalDate;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class RentalApplicationService {

    private static final Logger LOG =
            LoggerFactory.getLogger(RentalApplicationService.class);

    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private DateProvider dateProvider;

    public RentalApplicationService() {
    }

    @EventListener
    public void execute(StartRental command) {
        Customer customer =
                customerRepository.find(
                        new CustomerId(command.getCustomerId()));

        Rental rental =
                new Rental(
                        new RentalId(command.getRentalId()),
                        customer,
                        dateProvider);

        rentalRepository.save(rental);

        LOG.info("Rental started [{}]", command.getRentalId());
    }

    @EventListener
    public void execute(RentFilm command) {
        Film film = filmRepository.find(new FilmId(command.getFilmId()));

        update(command.getRentalId(), rental ->
                rental.rentFilm(film, command.getNbOfDays()));

        LOG.info("Film '{}' rented for {} days [{}]",
                film.title(),
                command.getNbOfDays(),
                command.getRentalId());
    }

    @EventListener
    public void execute(ReturnFilm command) {
        Film film = filmRepository.find(new FilmId(command.getFilmId()));

        update(command.getRentalId(), rental ->
                rental.returnFilm(
                        film,
                        LocalDate.parse(command.getReturnDate())));

        LOG.info("Film '{}' returned on '{}' [{}]",
                film.title(),
                command.getReturnDate(),
                command.getRentalId());
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void update(
            String rentalId,
            Consumer<Rental> execute) {

        Rental rental = rentalRepository.find(new RentalId(rentalId));

        execute.accept(rental);

        rentalRepository.save(rental);
    }
}
