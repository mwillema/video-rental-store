package com.marcowillemart.videorentalstore.application;

import com.marcowillemart.common.projection.QueryService;
import com.marcowillemart.videorentalstore.application.data.RentalData;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "rentals", path = "rentals")
public interface RentalQueryService extends QueryService<RentalData> {
}
