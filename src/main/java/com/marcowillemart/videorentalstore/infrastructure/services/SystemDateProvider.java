package com.marcowillemart.videorentalstore.infrastructure.services;

import com.marcowillemart.common.concurrent.GuardedBy;
import com.marcowillemart.common.concurrent.ThreadSafe;
import org.springframework.stereotype.Component;
import com.marcowillemart.videorentalstore.domain.DateProvider;
import java.time.LocalDate;

@Component
@ThreadSafe
public class SystemDateProvider implements DateProvider {

    @GuardedBy("this") private LocalDate lastDateProvided;

    @Override
    public LocalDate today() {
        synchronized (this) {
            lastDateProvided = LocalDate.now();
            return lastDateProvided;
        }
    }

    public LocalDate lastDateProvided() {
        synchronized (this) {
            return lastDateProvided;
        }
    }
}
