package com.marcowillemart.videorentalstore.infrastructure.repository;

import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.EventStoreRepository;
import com.marcowillemart.videorentalstore.domain.film.Film;
import com.marcowillemart.videorentalstore.domain.film.FilmId;
import com.marcowillemart.videorentalstore.domain.film.FilmRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EventStoreFilmRepository
        extends EventStoreRepository<Film, FilmId>
        implements FilmRepository {

    public EventStoreFilmRepository(EventStore eventStore) {
        super(eventStore, Film.class);
    }
}
