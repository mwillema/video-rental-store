package com.marcowillemart.videorentalstore.infrastructure.repository;

import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.EventStoreRepository;
import com.marcowillemart.videorentalstore.domain.customer.Customer;
import com.marcowillemart.videorentalstore.domain.customer.CustomerId;
import com.marcowillemart.videorentalstore.domain.customer.CustomerRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EventStoreCustomerRepository
        extends EventStoreRepository<Customer, CustomerId>
        implements CustomerRepository {

    public EventStoreCustomerRepository(EventStore eventStore) {
        super(eventStore, Customer.class);
    }
}
