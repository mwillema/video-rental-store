package com.marcowillemart.videorentalstore.infrastructure.repository;

import com.marcowillemart.common.event.EventStore;
import com.marcowillemart.common.event.EventStoreRepository;
import com.marcowillemart.videorentalstore.domain.rental.Rental;
import com.marcowillemart.videorentalstore.domain.rental.RentalId;
import com.marcowillemart.videorentalstore.domain.rental.RentalRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EventStoreRentalRepository
        extends EventStoreRepository<Rental, RentalId>
        implements RentalRepository {

    public EventStoreRentalRepository(EventStore eventStore) {
        super(eventStore, Rental.class);
    }
}
