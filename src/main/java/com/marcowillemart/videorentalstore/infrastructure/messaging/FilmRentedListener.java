package com.marcowillemart.videorentalstore.infrastructure.messaging;

import com.marcowillemart.videorentalstore.application.CommandProcessor;
import com.marcowillemart.videorentalstore.application.GrantBonusPoints;
import com.marcowillemart.videorentalstore.domain.rental.FilmRented;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class FilmRentedListener {

    @Autowired
    private CommandProcessor commandProcessor;

    public FilmRentedListener() {
    }

    @EventListener
    public void when(FilmRented event) {
        commandProcessor.process(
                GrantBonusPoints.newBuilder()
                        .setCustomerId(event.getCustomerId())
                        .setBonusPoints(event.getFilmBonusPoints())
                        .build());
    }
}
