package com.marcowillemart.videorentalstore.infrastructure.projection;

import com.marcowillemart.common.projection.AbstractProjection;
import com.marcowillemart.videorentalstore.application.data.FilmData;
import com.marcowillemart.videorentalstore.domain.film.FilmAdded;
import org.springframework.stereotype.Component;

@Component
public class FilmProjection extends AbstractProjection<FilmData> {

    void when(FilmAdded event) {
        FilmData film = new FilmData();

        film.setFilmId(event.getFilmId());
        film.setTitle(event.getTitle());
        film.setType(readableLabelFrom(event.getType()));

        save(film);
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private static String readableLabelFrom(FilmAdded.Type type) {
        switch (type) {
            case NEW:
                return "New release";
            case REGULAR:
                return "Regular film";
            case OLD:
                return "Old film";
            default:
                return type.name().toLowerCase();
        }
    }
}
