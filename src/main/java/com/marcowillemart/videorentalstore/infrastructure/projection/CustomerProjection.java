package com.marcowillemart.videorentalstore.infrastructure.projection;

import com.marcowillemart.common.projection.AbstractProjection;
import com.marcowillemart.videorentalstore.application.data.CustomerData;
import com.marcowillemart.videorentalstore.application.data.RentalData;
import com.marcowillemart.videorentalstore.application.data.RentedFilmData;
import com.marcowillemart.videorentalstore.domain.customer.BonusPointsGranted;
import com.marcowillemart.videorentalstore.domain.customer.CustomerCreated;
import com.marcowillemart.videorentalstore.domain.rental.FilmRented;
import com.marcowillemart.videorentalstore.domain.rental.FilmReturned;
import com.marcowillemart.videorentalstore.domain.rental.RentalStarted;
import java.util.function.Consumer;
import org.springframework.stereotype.Component;

@Component
public class CustomerProjection extends AbstractProjection<CustomerData> {

    void when(CustomerCreated event) {
        CustomerData customer = new CustomerData();

        customer.setCustomerId(event.getCustomerId());
        customer.setName(event.getName());
        customer.setBonusPoints(event.getInitialBonusPoints());

        save(customer);
    }

    void when(BonusPointsGranted event) {
        update(event.getCustomerId(),
                customer ->
                        customer.setBonusPoints(event.getTotalBonusPoints()));
    }

    void when(RentalStarted event) {
        update(event.getCustomerId(), customer -> {
            RentalData rental = new RentalData();

            rental.setRentalId(event.getRentalId());

            customer.getRentals().put(rental.getRentalId(), rental);
        });
    }

    void when(FilmRented event) {
        update(
                event.getCustomerId(),
                event.getRentalId(),
                rental -> {
                    rental.setTotalPrice(event.getTotalPrice());

                    RentedFilmData rentedFilm = new RentedFilmData();

                    rentedFilm.setFilmId(event.getFilmId());
                    rentedFilm.setDays(event.getDays());
                    rentedFilm.setPrice(event.getFilmPrice());
                    rentedFilm.setStatus("rented");

                    rental.getRentedFilms().add(rentedFilm);
                });
    }

    void when(FilmReturned event) {
        update(
                event.getCustomerId(),
                event.getRentalId(),
                rental -> {
                    rental.setTotalCharge(event.getTotalLateCharge());

                    RentedFilmData rentedFilm = rental.getRentedFilms()
                            .stream()
                            .filter(rf ->
                                    rf.getFilmId().equals(event.getFilmId()))
                            .findFirst()
                            .get();

                    rentedFilm.setExtraDays(event.getExtraDays());
                    rentedFilm.setLateCharge(event.getFilmLateCharge());
                    rentedFilm.setStatus("returned");
                });
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void update(
            String customerId,
            String rentalId,
            Consumer<RentalData> execute) {

        update(customerId, customer -> {
            execute.accept(customer.getRentals().get(rentalId));
        });
    }
}
