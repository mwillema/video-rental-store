package com.marcowillemart.videorentalstore.infrastructure.projection;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SchemaVersion implements Serializable {

    @Id
    @Column(name = "v_1")
    private int schemaVersion;
}
